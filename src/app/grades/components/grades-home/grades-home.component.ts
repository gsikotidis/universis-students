import { Component, OnInit } from '@angular/core';
import {GradesService} from "../../services/grades.service";

@Component({
  selector: 'app-grades-home',
  templateUrl: './grades-home.component.html',
  styleUrls: ['./grades-home.component.scss']
})
export class GradesHomeComponent implements OnInit {

  constructor(private gradeService: GradesService) { }

  ngOnInit() {

  }

}
