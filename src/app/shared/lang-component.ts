import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router, RouterState} from "@angular/router";
import {ConfigurationService} from "./services/configuration.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: 'app-lang-msgbox',
    template: '<div></div>'
})
export class LangComponent implements OnInit {

    public previousUrl: string;

    constructor(private _config: ConfigurationService,
                private _translate: TranslateService,
                private _router: Router,
                private activatedRoute:ActivatedRoute) {
        this._router.events
            .filter(event => event instanceof NavigationEnd)
            .subscribe( (ev:any) => {
                this.previousUrl = ev.url;
            });
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params)=> {
            localStorage.setItem('currentLang', params.id);
            this._translate.use(params.id);
            this._router.navigate(['/']);
        });
    }



}
