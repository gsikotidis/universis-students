import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-msgbox',
  templateUrl: './msgbox.component.html',
  styleUrls: ['./msgbox.component.scss']
})
export class MsgboxComponent {

  @Input() title: string;
  @Input() icon: string;
  @Input() info: string;
  @Input() message: string;
  @Input() extraMessage: string;
  @Input() actionButton: string;
  @Input() actionText: string;
  @Input() disableBut: boolean;
  // Usage (action)="someFunction()"
  @Output() action = new EventEmitter<any>();

  btnClicked: boolean;

  clicked() {
    if (!this.btnClicked) {
      this.action.emit();
      this.btnClicked = true;
    }
  }
}
