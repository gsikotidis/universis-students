import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationCheckoutComponent } from './registration-checkout.component';

describe('RegistrationCheckoutComponent', () => {
  let component: RegistrationCheckoutComponent;
  let fixture: ComponentFixture<RegistrationCheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationCheckoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
